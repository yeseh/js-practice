const meetingsRouter = require('express').Router()

const { 
    addToDatabase,
    getAllFromDatabase,
    getFromDatabaseById,
    updateInstanceInDatabase,
    deleteFromDatabasebyId,
    deleteAllFromDatabase,
    createMeeting
  } = require('./db');

  module.exports = meetingsRouter;

  meetingsRouter.get('/', (req, res, next) => {
    res.send(getAllFromDatabase('meetings'))
    });

  meetingsRouter.post('/', (req, res, next) => {
      const newMeeting = addToDatabase('meetings', createMeeting());
      res.status(201).send(newMeeting);
    })

  meetingsRouter.delete('/', (req,res,next) => {
      const deletedMeetings = deleteAllFromDatabase('meetings');
      res.status(204).send();
  })