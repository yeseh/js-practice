const  ideasRouter = require('express').Router()
const checkIdea = require('./checkMillionDollarIdea')

const { 
    addToDatabase,
    getAllFromDatabase,
    getFromDatabaseById,
    updateInstanceInDatabase,
    deleteFromDatabasebyId,
    isValidIdea
  } = require('./db');

  module.exports = ideasRouter;

  ideasRouter.param('ideaId', (req, res, next, id) =>{
    const idea = getFromDatabaseById('ideas', id);
      if (idea){
        req.idea = idea;
        next();
      } else {
        res.status(404).send();
      }
  });

  ideasRouter.get('/', (req, res, next) => {
  res.send(getAllFromDatabase('ideas'))
  });

ideasRouter.get('/:ideaId', (req, res, next) => {
  res.send(req.idea);
  });

ideasRouter.put('/:ideaId', checkIdea, (req, res, next) => {
  const updatedIdea = updateInstanceInDatabase('ideas', req.body);
  res.send(updatedIdea);
  });

ideasRouter.post('/', checkIdea, (req, res, next) => {
  const newIdea = addToDatabase('ideas', req.body);
  res.status(201).send(newIdea);
  })

ideasRouter.delete('/:ideaId', (req, res, next) => {
const deletedIdea = deleteFromDatabasebyId('ideas', req.params.ideaId);
res.status(204).send();
})