const minionsRouter = require('express').Router();

module.exports = minionsRouter

const { 
    addToDatabase,
    getAllFromDatabase,
    getFromDatabaseById,
    updateInstanceInDatabase,
    deleteFromDatabasebyId,
    isValidMinion
  } = require('./db');

minionsRouter.param('minionId', (req, res, next, id) =>{
  const minion = getFromDatabaseById('minions', id);
    if (minion){
      req.minion = minion;
      next();
    } else {
      res.status(404).send();
    }
});

minionsRouter.get('/', (req, res, next) => {
    res.send(getAllFromDatabase('minions'))
    });

minionsRouter.get('/:minionId', (req, res, next) => {
  res.send(req.minion);
});

minionsRouter.put('/:minionId', (req, res, next) => {
  const updatedMinion = updateInstanceInDatabase('minions', req.body);
  console.log(req.body);
  res.send(updatedMinion);
});

minionsRouter.post('/', (req, res, next) => {
  if (isValidMinion(req.body)) {
  const newMinion = addToDatabase('minions', req.body);
  res.status(201).send(newMinion);
  }
})

minionsRouter.delete('/:minionId', (req, res, next) => {
  const deletedMinion = deleteFromDatabasebyId('minions', req.params.minionId);
  res.status(204).send(deletedMinion);
})