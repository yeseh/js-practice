/*
#### Routes Required

- `/api/meetings`
  - GET /api/meetings to get an array of all meetings.
  - POST /api/meetings to create a new meeting and save it to the database.
  - DELETE /api/meetings to delete _all_ meetings from the database.
  */

const express = require('express');
const app = express();

module.exports = app;
const PORT = process.env.PORT || 4001;

const cors = require('cors');
app.use(cors());

const bodyParser = require('body-parser');
app.use(bodyParser.json());

const apiRouter = require('./server/api');
app.use('/api', apiRouter);

if (!module.parent) { 
  app.listen(PORT, () => console.log(`Server is listening on Port ${PORT}`));
}