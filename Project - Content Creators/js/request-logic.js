function getContentType(filename) {
  let extension = filename.match(/.*\.([^\.]*)$/)[1];
  if (extension === 'html' || extension === 'css'){
    return 'text/' + extension;
  } else if (extension === 'jpeg' || extension === 'jpg') {
    return 'image/jpeg';
  }
    return 'text/plain';  
  }


