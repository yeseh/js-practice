// Use this presets array inside your presetHandler
const presets = require('./presets');

// Complete this function:
const checkValidIndex = index => {
    if (index < presets.length && index >= 0){
        return true;
    } else {
        return false;
    }
}

const checkValidRequest = type => {
    if (type === 'GET' || type === 'PUT'){
        return true;
    } else {
        return false;
    }
}

const presetHandler = (requestType, presetIndex, newPresetArray) => {
    let presetArray = [];
    if (!checkValidRequest(requestType)) {
        presetArray[0] = 400;
        return presetArray;
    }
    if (!checkValidIndex(presetIndex)) {            
        presetArray[0] = 404;
        return presetArray;
    } else if (checkValidIndex(presetIndex)) {
        presetArray[0] = 200;
        if (requestType === 'GET'){
            presetArray[1] = presets[presetIndex];
            return presetArray; 
        } else if (requestType === 'PUT'){
            presets[presetIndex] = newPresetArray;
            presetArray[1] = presets[presetIndex];
            return presetArray;
        }
    }    
};

// Leave this line so that your presetHandler function can be used elsewhere:
module.exports = presetHandler;
