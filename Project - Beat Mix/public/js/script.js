// Drum Arrays

let kicks = new Array(16).fill(false);
let snares = new Array(16).fill(false);
let hiHats = new Array(16).fill(false);
let rideCymbals = new Array(16).fill(false);
let drumContainer = {kicks: kicks, 
                 snares: snares, 
                 hiHats: hiHats,
                 rideCymbals: rideCymbals};
let checks = 0;

const ValidateArray = drumArray => {
    if (drumArray === 'kicks' || drumArray === 'snares' 
        || drumArray === 'hiHats' || drumArray === 'rideCymbals') {
            return true;
        } else {
            return false;
        }
}
const toggleDrum = (drumArray, index) => { 
drumArray = drumContainer[drumArray];
    if(drumArray && index < drumArray.length - 1 && index >= 0){
        if(!drumArray[index]) {
            drumArray[index] = true;
        } else {
            drumArray[index] = false;  
        }
    };
}

const clear = drumArray => {
    if (ValidateArray(drumArray))
    {   
        drumArray = drumContainer[drumArray];
        drumArray.fill(false);
    }   
}
const invert = drumArray => {
    if (ValidateArray(drumArray))
        {
        drumArray = drumContainer[drumArray];
        drumArray.forEach((value, index) => 
        {
            if (drumArray[index]){
                drumArray[index] = false;
            } else {
                drumArray[index] = true;
            }
        }); 
    }
}
const Check = () => {
    checks++;
    console.log(`Check ${checks}`);
}
const getNeighborPads = (x, y, size) => {
    result = [];
    if (x > size-1 || y > size-1 || x < 0 || y < 0) return result;
    // Hoek linksonder
    if (x === 0 && y === 0){
        result.push([x, y+1], [x+1,y]);
    }
    // Hoek Rechtsboven
    if (x === 4 && y === 4){
        result.push([x, y-1], [x-1,y]);
    } 
    // Hoek linksboven
    if (x === 0 && y === 4){
        result.push([x, y-1], [x+1,y]);
    }
    // Hoek Rechtsonder
    if (x === 4 && y === 0){
        result.push([x-1, y], [x, y+1]);
    } 
    // Onderste rij - geen hoeken
    if (x !== 0 && y === 0 && x !== 4){
        result.push([x-1, y],[x+1, y],[x, y+1]);
    }
    // Bovenste rij - geen hoeken
    if (x !== 0 && y === 4 && x !== 4){
        result.push([x-1, y],[x+1, y],[x, y-1]);
    }
    // rechter rij - geen hoeken
    if (x === 4 && y !== 0 && y !== 4){
        result.push([x-1, y],[x, y-1],[x, y+1]);
    }
    // linker rij - geen hoeken
    if (x === 0 && y !== 0 && y !== 4){
        result.push([x+1, y],[x, y-1],[x, y+1])
}   if (x !== 0 && x !== 4 && y !== 0 && y !== 4){
    result.push([x+1, y], [x-1, y], [x, y-1], [x, y+1]);
}
    return result;
}
