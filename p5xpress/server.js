const express = require('express');
const bodyParser = require('body-parser')
const errorhandler = require('errorhandler')
const cors = require('cors')
const morgan = require('morgan')
const apiRouter = require('./api/api')

const app = express();
const PORT = process.env.PORT || 4000;

app.use(cors());
app.use(errorhandler());
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use('/api', apiRouter);

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
});

module.exports = app;


