const express = require('express');
const sqlite3 = require('sqlite3');

const artistRouter = express.Router();
const db = new sqlite3.Database('./database.sqlite');

artistRouter.param('artistId', (req, res, next, artistId) => {
    db.get('SELECT * FROM Artist WHERE Artist.id = $artistId', {$artistId: artistId}, (err, artist) =>{
        if(err){
            next (err);
        } else if (artist) {
            req.artist = artist;
            next();
        } else {
            res.status(404).send(`404: Artist with id ${artistId} not found`)
        }
    })
})
artistRouter.get('/', (req,res,next) => {
    db.all("SELECT * FROM Artist WHERE is_currently_employed = 1", (err, artists) =>{
        if(err) {
            next(err);
        } else {
            res.status(200).send({artists: artists});
        }
    })
})
artistRouter.get('/:artistId', (req, res, next) => {
    res.status(200).send({artist: req.artist});
})
artistRouter.post('/', (req, res, next) => {
    if(req.body.artist.name && req.body.artist.dateOfBirth && req.body.artist.biography){
        req.body.artist.isCurrentlyEmployed === 0 ? 0 : 1;
        db.serialize(() => { 
            let sql = "INSERT INTO Artist (name, date_of_birth, biography) VALUES ($name, $dob, $bio);"
            let values = {
                $name: req.body.artist.name, 
                $dob: req.body.artist.dateOfBirth, 
                $bio: req.body.artist.biography}
            db.run(sql, values, (error) =>{
                if(error){
                    next(error);
                }
            });
            db.get(`SELECT * from ARTIST WHERE Artist.id = ${this.lastID}`, (error, artist) => {
                console.log(artist);
                if (error){ next(error); } 
                else { res.status(201).json({ artist: artist }) }
            })
        })
    } else {
        res.status(400).send()
    }
});

module.exports = artistRouter;