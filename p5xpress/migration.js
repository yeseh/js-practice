const sqlite3 = require('sqlite3');
const db = new sqlite3.Database('./database.sqlite');

db.serialize(()=>{
    db.run("DROP TABLE IF EXISTS Artist");
    db.run("CREATE TABLE Artist (id integer primary key not null, name varchar(50) not null, date_of_birth varchar(50) not null, biography varchar(1000) not null, is_currently_employed default 1)", 
            error => { if (error) {
                console.log(error);
                }
            })
        });
db.serialize(()=>{
    db.run("DROP TABLE IF EXISTS Series");
    db.run("CREATE TABLE Series (id integer primary key not null, name varchar(50) not null, description varchar(255) not null)", 
            error => { if (error) {
                console.log(error);
                }
            })
        });
db.serialize(()=>{
    db.run("DROP TABLE IF EXISTS Issue");
    db.run("CREATE TABLE Issue (id integer primary key not null, name varchar(50) not null, issue_number integer not null, publication_date varchar(255) not null, artist_id integer not null, series_id integer not null, FOREIGN KEY(artist_id) references Artist(id),FOREIGN KEY(series_id) references Series(id));",
            error => { if (error) {
                console.log(error);
                }
            })
        });