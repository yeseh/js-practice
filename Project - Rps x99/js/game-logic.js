//==================
// Global Variables
//==================

let player;
let roundNumber;
let playerOneWins;
let playerTwoWins;

//Player one
let playerOneMoveOneValue; 
let playerOneMoveTwoValue;
let playerOneMoveThreeValue;

let playerOneMoveOneType;
let playerOneMoveTwoType;
let playerOneMoveThreeType;

// Player two
let playerTwoMoveOneValue;
let playerTwoMoveTwoValue;
let playerTwoMoveThreeValue;

let playerTwoMoveOneType;
let playerTwoMoveTwoType;
let playerTwoMoveThreeType;



//=========================


const setPlayerMoves = (player, moveOneType, moveOneValue, moveTwoType, moveTwoValue, moveThreeType, moveThreeValue) => 
{
    if(checkInputIntegrity(moveOneType, moveOneValue, moveTwoType, moveTwoValue, moveThreeType, moveThreeValue)){
        if(player === 'Player One'){
            setPlayerOneMoves(moveOneType, moveOneValue, moveTwoType, moveTwoValue, moveThreeType, moveThreeValue);
        }else if (player === 'Player Two') { 
            setPlayerTwoMoves(moveOneType, moveOneValue, moveTwoType, moveTwoValue, moveThreeType, moveThreeValue);
        } else {console.log('ERROR: INVALID PLAYER');}
    }
}
const checkInputIntegrity = (moveOneType, moveOneValue, moveTwoType, moveTwoValue, moveThreeType, moveThreeValue) => {
    if(moveOneType === 'rock' || moveOneType === 'scissors' || moveOneType === 'paper') { // check move 1 types
        if (moveTwoType === 'rock' || moveTwoType === 'scissors' || moveTwoType === 'paper') {// check move 2 types
            if (moveThreeType === 'rock' || moveThreeType === 'scissors' || moveThreeType === 'paper') { // check move 3 types
                if(moveOneValue > 0 && moveTwoValue > 0 && moveThreeValue > 0) { // values need to be > 0
                    if (moveOneValue < 99 || moveTwoValue < 99 || moveThreeValue < 99) { // values need to be < 99
                        if(moveOneValue + moveTwoValue + moveThreeValue <= 99){ // sum of values needs to be < 99
                            console.log('Input is valid');
                            return true;
                        }
                    }
                }
            }
        }
    } 
    console.log('Input is not valid');
    return false;
}
const setPlayerOneMoves = (moveOneType, moveOneValue, moveTwoType, moveTwoValue, moveThreeType, moveThreeValue) => 
{   
    playerOneMoveOneValue = moveOneValue; 
    playerOneMoveTwoValue = moveTwoValue;
    playerOneMoveThreeValue = moveThreeValue;
    playerOneMoveOneType = moveOneType;
    playerOneMoveTwoType = moveTwoType;
    playerOneMoveThreeType = moveThreeType;
}
const setPlayerTwoMoves = (moveOneType, moveOneValue, moveTwoType, moveTwoValue, moveThreeType, moveThreeValue) => 
{   
    playerTwoMoveOneValue = moveOneValue; 
    playerTwoMoveTwoValue = moveTwoValue;
    playerTwoMoveThreeValue = moveThreeValue;
    playerTwoMoveOneType = moveOneType;
    playerTwoMoveTwoType = moveTwoType;
    playerTwoMoveThreeType = moveThreeType;
       
}
const setComputerMoves = () => 
{
 // Test how computer player is registered
}

const getRoundWinner = (roundNumber) => 
{
    switch (roundNumber) {
        case 1:
            console.log(playerOneMoveOneType);
            console.log(playerOneMoveOneValue);
            return getMoveWinner(playerOneMoveOneType, 
                        playerOneMoveOneValue,
                        playerTwoMoveOneType,
                        playerTwoMoveOneValue);
        case 2:
            return getMoveWinner(playerOneMoveTwoType,
                        playerOneMoveTwoValue,
                        playerTwoMoveTwoType,   
                        playerTwoMoveTwoValue);
        case 3:
            return getMoveWinner(playerOneMoveThreeType,
                        playerOneMoveThreeValue,
                        playerTwoMoveThreeType,
                        playerTwoMoveThreeValue);
        default:
            console.log('Invalid Round Number');
            return null;
    }
}
const getMoveWinner = (p1MoveType, p1MoveValue, p2MoveType, p2MoveValue) => {
    if (!p1MoveType || !p1MoveValue || !p2MoveType || !p2MoveValue){
        return null;
    }
    if (p1MoveType === p2MoveType){
        if (p1MoveValue > p2MoveValue) {
            return 'Player One';
        } else if (p1MoveValue < p2MoveValue){
            return 'Player Two';
        } else if (p1MoveValue == p2MoveValue) {
            return 'Tie';
        } 
    }
    if (p1MoveType === 'rock') {
        if (p2MoveType === 'scissors'){
            return 'Player One';
        } else if (p2MoveType === 'paper'){
            return 'Player Two';
        }
    }
    if (p1MoveType === 'scissors'){
        if (p2MoveType === 'paper'){
            return 'Player One';
        } else if (p2MoveType === 'rock') {
            return 'Player Two';
        }
    }
    if (p1MoveType === 'paper'){
        if (p2MoveType === 'rock'){
            return 'Player One';
        } else if (p2MoveType === 'scissors'){
            return 'Player Two';
        }
}
}

const getGameWinner = () => 
{
    if (!playerOneMoveOneType || !playerOneMoveOneValue ||
        !playerOneMoveTwoType || !playerOneMoveTwoValue ||
        !playerOneMoveThreeType || !playerOneMoveThreeValue ||
        !playerTwoMoveOneType || !playerTwoMoveOneValue ||
        !playerTwoMoveTwoType || !playerTwoMoveTwoValue ||
        !playerTwoMoveThreeType || !playerTwoMoveThreeValue) 
        {
            return null;
        }
    playerOneWins = 0;
    playerTwoWins = 0;
            
    const roundOneWinner = getRoundWinner(1);
    const roundTwoWinner = getRoundWinner(2);
    const roundThreeWinner = getRoundWinner(3);

    addWins(roundOneWinner);
    addWins(roundTwoWinner);
    addWins(roundThreeWinner);

    console.log(`The winner of round 1 is ${roundOneWinner}`);
    console.log(`The winner of round 2 is ${roundTwoWinner}`);
    console.log(`The winner of round 3 is ${roundThreeWinner}`);

    if(playerOneWins > playerTwoWins){
        return 'Player One';
    }
    else if (playerOneWins < playerTwoWins){
        return 'Player Two';
    } else if (playerOneWins == playerTwoWins){
        return 'Tie';
    }
    }
const addWins = (winner) =>{
    if (winner === 'Player One'){
        playerOneWins++ || 1;
    } else if (winner === 'Player Two'){
        playerTwoWins++ || 1;
    } else if (winner === 'Tie');
        return null;
}